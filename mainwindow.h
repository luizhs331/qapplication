#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QPushButton>
#include <QApplication>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void abrirJanela( QWindow* window );

private slots:
    void on_pushButton_clicked();
//    void onAbrirJanela( QWindow* window );
    void onAbrirJanela();

    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui;
    QList<QPushButton*> _buttonList = {};
    QWindow* _window =  nullptr;
    QApplication* _application = nullptr;
};
#endif // MAINWINDOW_H

