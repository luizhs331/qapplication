#include "mainwindow.h"
#include "secondwindow.h"
#include "widgetwindow.h"

#include <QDebug>

#include <QApplication>
#include <QDesktopWidget>
#include <QStyleFactory>
#include <QQuickStyle>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QDesktopWidget* desktopScreen = a.desktop();

    MainWindow *primeiraWindow = new MainWindow( desktopScreen->window() );
    primeiraWindow->setStyle( QStyleFactory::create("Fusion") );

    SecondWindow* segundaWindow = new SecondWindow() ;
    WidgetWindow* widgetWindow = new WidgetWindow();

    primeiraWindow->setWindowTitle("Primeira Janela");
    segundaWindow->setWindowTitle("Segunda Janela");
    widgetWindow->setWindowTitle("Terceira Janela");

    primeiraWindow->show();
    segundaWindow->show();
    widgetWindow->show();

    widgetWindow->setGeometry( desktopScreen->availableGeometry() );

    QWidgetList listaDeWidget = a.allWidgets();

    qDebug() << "Widgets Ativas no Sistema:" << listaDeWidget.count();

    // a.aboutQt();

    return a.exec();

}
