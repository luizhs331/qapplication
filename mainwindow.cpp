#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QWindow>
#include <QDebug>

#define JANELAS_NAO_TRATAR 2

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->pushButton->setFixedWidth( 100 );
    ui->pushButton->setFixedHeight( 50 );
}

MainWindow::~MainWindow()
{
    delete ui;
}

//void MainWindow::onAbrirJanela( QWindow *window ){}

void MainWindow::onAbrirJanela(){
    _window->show();
}


void MainWindow::on_pushButton_clicked(){

    QApplication* application;

    QWindowList todasAsJanelas = application->allWindows();
    QWidgetList todasAsWidgets = application->allWidgets();


    qDebug()<< "Todas As Widgets" << todasAsWidgets.count();

    int i = 0;

    qDebug() << "Nome Aaplication:" << QCoreApplication::instance()->applicationName();

    application->alert( application->widgetAt( 0 , 1 ) );

//     ui->verticalLayout = new QVBoxLayout();
//     ui->verticalLayout->setAlignment( Qt::AlignHCenter );

    if( ( ui->verticalLayout->count() + JANELAS_NAO_TRATAR ) < application->allWindows().count() ){

    for ( QWindow* janela : application->allWindows() ) {

        QPushButton* button = new QPushButton();

        if( !janela->title().trimmed().isEmpty() ){

        button->setText( janela->title() );

        button->setFixedWidth( 100 );
        button->setFixedHeight( 50 );

        if( !todasAsJanelas.indexOf( janela ) ){
        _window = janela;
        }

        _buttonList.append( button );

        ui->verticalLayout->addWidget( button );

        qDebug() << "Signals Blocked:" << QCoreApplication::instance()->blockSignals( true );

        QObject::connect( _buttonList.at( i ),  &QPushButton::clicked, this,  &MainWindow::onAbrirJanela );

        i++;

      }
    }
   }

   // QWindow *terceiraJanela = teste.at( 0 );

 //   terceiraJanela->show();

//    QWidget* widget = application->widgetAt( 0 , 2 );

//    application->setActiveWindow( );

}

void MainWindow::on_pushButton_3_clicked(){
    _application->aboutQt();
}

void MainWindow::on_pushButton_2_clicked(){
    _application->closeAllWindows();
}
